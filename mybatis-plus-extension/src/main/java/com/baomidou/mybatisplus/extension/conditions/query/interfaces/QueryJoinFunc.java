/*
 * Copyright (c) 2011-2022, baomidou (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.baomidou.mybatisplus.extension.conditions.query.interfaces;

import com.baomidou.mybatisplus.core.toolkit.Constants;

/**
 * join封装
 *
 * @author mahuibo
 * @since 2022/11/23
 */
public interface QueryJoinFunc<Children> {

    default Children innerJoin(Class<?> joinClass) {
        return join(joinClass, Constants.INNER_JOIN);
    }

    default Children leftJoin(Class<?> joinClass) {
        return join(joinClass, Constants.LEFT_JOIN);
    }

    default Children rightJoin(Class<?> joinClass) {
        return join(joinClass, Constants.RIGHT_JOIN);
    }

    /**
     * join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @param joinType  join类型 inner join left join right join
     * @return this
     */
    Children join(Class<?> joinClass, String joinType);

}
